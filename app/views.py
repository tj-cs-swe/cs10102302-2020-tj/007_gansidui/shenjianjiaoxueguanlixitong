import os
import imghdr
from PIL import Image
from flask import render_template, flash, redirect, url_for, request, session
from flask_login import login_user, logout_user, current_user, login_required
from app import app, db, login, SuffixCommand, SuffixLanguage, Problem_Details_Class, JudgeResult
from app.forms import *
from app.models import Student, Teacher, Admin, Problem, Record, Post, Comment, Notice
from sqlalchemy import and_, text
from datetime import datetime
from app.OJ_Module.judge_code import PROBLEM_DIR, RECORD_DIR,Source_Code
from flask_wtf.csrf import validate_csrf
from wtforms import ValidationError
from app.OJ_Module.judge import Get_Task
from flask_paginate import Pagination, get_page_parameter

from concurrent.futures import ThreadPoolExecutor
from numpy import unicode
executor = ThreadPoolExecutor(4)

'''@app.before_request
def before_request():
'''

'''
@oid.after_login
def after_login(resp):
    return redirect(url_for('index'))
'''

def getCoding(strInput):
    '''
    获取编码格式
    '''
    if isinstance(strInput, unicode):
        return "unicode"
    try:
        strInput.decode("utf8")
        return 'utf8'
    except:
        pass
    try:
        strInput.decode("gbk")
        return 'gbk'
    except:
        pass

def tran2UTF8(strInput):
    '''
    转化为utf8格式
    '''
    strCodingFmt = getCoding(strInput)
    if strCodingFmt == "utf8":
        return strInput
    elif strCodingFmt == "unicode":
        return strInput.encode("utf8")
    elif strCodingFmt == "gbk":
        return strInput.decode("gbk").encode("utf8")

def tran2GBK(strInput, strCodingFmt):
    '''
    转化为gbk格式
    '''
    if strCodingFmt == "gbk":
        return strInput
    elif strCodingFmt == "unicode":
        return strInput.encode("gbk")
    elif strCodingFmt == "utf8":
        return strInput.decode("utf8").encode("gbk")
    
def tran2(strInput, strCodingFmt):
    if strCodingFmt == 'gbk':
        return strInput.decode('gbk')
    elif strCodingFmt == 'unicode':
        return strInput.encode('gbk')
    elif strCodingFmt == 'utf8':
        return strInput.decode("utf8").encode("gbk").decode('gbk')



@login.user_loader
def load_user(id):
    #不同身份用户的id不相同，无需判断session
    if Student.query.get(id):
        return Student.query.get(id)
    elif Teacher.query.get(id):
        return Teacher.query.get(id)
    else :
        return Admin.query.get(id)


@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
def index():
    
    return render_template("index.html",
                           title='主页',
                           user=None)


@app.route('/oj')
def oj():
    return render_template("index.html",
                           title='Open Judge')

def countSuffixFile(recordID):
    filedir = os.path.join(RECORD_DIR,str(recordID))
    filenames = os.listdir(filedir)
    count = 0
    for filename in filenames:
	    filesuffix = filename.rsplit('.',1)[1]
	    if filesuffix in SuffixLanguage:
		    count = count+1
    return count


# 筛选我的提交
# todo 将提交记录的代码加入recordList
# todo 修改提交时间现实方式
@app.route('/oj/mysubmit', methods=['GET', 'POST'])
@login_required
def oj_mysubmit():
    # Judge
    executor.submit(Get_Task)

    form = SubmitResultForm()
    records = []
    # 表单提交合法
    if form.validate_on_submit():
        # 按照题目编号、语言、提交状态筛选
        record = Record.query.filter(and_(Record.Student_ID == current_user.Student_ID,
            Record.Problem_ID == form.problemId.data if form.problemId.data != "" else text(""),
            Record.Language == SuffixLanguage[form.language.data] if form.language.data != "all" else text(""),
            Record.Status == form.result.data if form.result.data != "all" else text(""))).order_by(Record.Submit_Time.desc()).all()
            # 如果查询结果为空，则给出提示
        if record == []:
            flash("查询为空，请重新查询")
            return render_template("mysubmit.html",title='Open Judge - 我的提交',form=form)
    else: # 缺省为显示所有记录
        record = Record.query.filter(Record.Student_ID == current_user.Student_ID).order_by(Record.Submit_Time.desc()).all()
    # flash (record)

    # Add recordList
    for r in record:
        problem = Problem.query.get(r.Problem_ID)
        # fetch full code
        count = countSuffixFile(r.Record_ID)
        # multifile
        if count == 1:
            with open(os.path.join(RECORD_DIR,str(r.Record_ID),Source_Code[SuffixCommand[r.Language]]),'rb') as file:
                code = file.read()
                strCodingFmt = getCoding(code)
                code = tran2GBK(code,strCodingFmt).decode('gbk')
                # print(code)
        else:
            code = "多文件提交，无法查看代码"
        # CE error info
        errordir = os.path.join(RECORD_DIR,str(r.Record_ID),'error.log')
        if os.path.exists(errordir):
            with open(os.path.join(RECORD_DIR,str(r.Record_ID),'error.log'),'r', encoding = 'unicode_escape') as file:
                error = file.read()
        else:
            error = ""
        Time_Consume = format(r.Time_Consume,'.4f')
        Memory_Consume = format(r.Memory_Consume,'.4f')
        records.append({"Record":r,"TimeLimit":Time_Consume,"MemoryLimit":Memory_Consume,"Code":code,"JudgeResult":JudgeResult[r.Status], "CEINFO":error})

    curPage = request.args.get(get_page_parameter(), type=int, default=1)
    perPage = 10
    recordList = records[(curPage - 1) * perPage : curPage * perPage]
    pagination = Pagination(
        page = curPage,
        per_page = perPage,
        total = len(records),
        bs_version=4
    )

    # Judge
    #Get_Task()

    return render_template(
        "mysubmit.html",
        title='Open Judge - 我的提交',
        form=form,
        recordList=recordList,
        pagination=pagination
    )


# 题目列表
# todo models.py-Problem中添加发布时间、截止时间
# todo 按照题目的属性(Problem_ID, Latest_Time)进行筛选
@app.route('/oj/problems')
@login_required
def oj_problems():
    problems = Problem.query.all()
    problems_wiz_teaname = []
    for problem in problems:
        new_problem = {}
        new_problem['Problem_Id'] = problem.Problem_ID
        new_problem['Problem_Name'] = problem.Problem_Name
        teacher = Teacher.query.filter(Teacher.Teacher_ID == problem.Teacher_ID).first()
        new_problem['Teacher_Name'] = teacher.TeaName
        problems_wiz_teaname.append(new_problem)

    curPage = request.args.get(get_page_parameter(), type=int, default=1)
    perPage = 10
    problemList = problems_wiz_teaname[(curPage - 1) * perPage : curPage * perPage]
    pagination = Pagination(
        page = curPage,
        per_page = perPage,
        total = len(problems),
        bs_version=4
    )

    return render_template(
        "problems.html",
        title='Open Judge - 题目列表',
        problemList=problemList,
        pagination = pagination
    )


# 题目详情页
@app.route('/oj/problem/<int:Problem_ID>')
@login_required
def problem(Problem_ID):

    if Problem_ID != "" and request.method == 'GET':
        Problem_Details_Dir = os.path.join(PROBLEM_DIR,str(Problem_ID))
        # Problem_Details_Class = ['Background','Description', 'InputFormat', 'OutputFormat', 'SampleInput', 'SampleOutput']
        Problem_Details = {}
        problem = Problem.query.get(Problem_ID)
        Problem_Details['Problem_ID'] = problem.Problem_ID
        Problem_Details['Problem_Name'] = problem.Problem_Name
        teacher = Teacher.query.get(problem.Teacher_ID)
        Problem_Details['TeacherName'] = teacher.TeaName
        Problem_Details['TimeLimit'] = problem.Time_Limit
        Problem_Details['MemoryLimit'] = problem.Memory_Limit
        for detail in Problem_Details_Class:
            with open(os.path.join(Problem_Details_Dir,detail) + '.txt','r') as file:
                Problem_Details[detail] = file.read()
                # print (detail +':'+ Problem_Details[detail])
        return render_template("problem.html",title='Open Judge - 查看题目',details = Problem_Details)

    return render_template("problem.html",title='Open Judge - 题目详情')


# 代码提交页面
# todo 文件上传功能与后端衔接
@app.route('/oj/submit/<int:Problem_ID>', methods=['GET', 'POST'])
@login_required
def oj_submit(Problem_ID):
    # 已经在form和前端中添加文件选项
    # 注意 文件上传优先级高于文本字段
    form = SubmitForm()
    # flash(form.validate_on_submit())
    if current_user.Identification == 'Student' and form.validate_on_submit():
        
        # 验证CSRF令牌
        try:
            validate_csrf(form.csrf_token.data)
        except ValidationError:
            flash('CSRF token error.')
            return redirect(url_for('oj_mysubmit'))

        # add record
        language = form.compiler.data
        Student_ID = current_user.Student_ID
        now = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        # address uploadfile
        filename = form.uploadfile.data
        if filename[0].filename == "":
            code = form.code.data
            codeLength = len(code)
        else:   # 文件上传
            filecontentList = []
            # 检查文件的语言是否合法
            #fileSuffix = filename.rsplit('.',1)[1]
            #if fileSuffix not in SuffixLanguage:
            #    return render_template("submit.html")
            codeLength = 0
            for f in request.files.getlist('uploadfile'):
                filecontent = f.read()
                strCodingFmt = getCoding(filecontent)
                filecontent = tran2UTF8(filecontent)
                filecontentList.append(filecontent)
                codeLength += len(filecontent)
        # flash("代码提交成功")
    
        # add record to DataBase
        record = Record(Student_ID = Student_ID, Problem_ID = Problem_ID,
                        Status = "WAITING", Submit_Time = now, Language = language, CodeLength = codeLength, 
                        Time_Consume = 0.0, Memory_Consume = 0.0)
        db.session.add(record)
        #获取自增的Post_ID
        db.session.flush()
        db.session.commit()
        # commit后自增的id已经获得了，可以直接使用record.Record_ID
        
        # write code into files if filename is None
        recordDir = os.path.join(RECORD_DIR, str(record.Record_ID))
        if not os.path.exists(recordDir): # mkdir
            os.mkdir(recordDir)

        if filename[0].filename != "":
            for content in filecontentList:
                # flash(content)
                with open(os.path.join(recordDir,Source_Code[SuffixCommand[language]]),'wb') as file:
                    file.write(content)
        else:
            with open(os.path.join(recordDir,Source_Code[SuffixCommand[language]]),'wb') as file:
                code = code.encode('UTF-8')
                file.write(code)
        return redirect(url_for('oj_mysubmit'))
        #return render_template("submit.html", title='Open Judge - 提交题目',form=form)

    return render_template("submit.html", title='Open Judge - 提交题目',form=form)


# 增加OJ题目
# 目前页面最后仍停留在本页面，可按需调整
@app.route('/oj/add_problem', methods=['GET', 'POST'])
def addOJProblem():

    form = AddProblemForm()
    # flash(form.validate_on_submit())
    # 设定只有老师(或管理员)有资格增加题目
    if current_user.Identification == 'Teacher' and form.validate_on_submit():
        # 验证CSRF令牌
        try:
            validate_csrf(form.csrf_token.data)
        except ValidationError:
            flash('CSRF token error.')
            return redirect(url_for('multi_upload'))
        
        # INSERT INTO TABLE problem
        problem = Problem(Problem_Name = form.problemName.data,
            Teacher_ID = current_user.Teacher_ID,
            Time_Limit = form.timeLimitation.data,
            Memory_Limit = form.memoryLimitation.data)
        db.session.add(problem)
        db.session.flush()
        db.session.commit()
        # commit后problem已经获得自增主键id，可直接使用

        # Add problem_details to local files
        problem_dir = os.path.join(PROBLEM_DIR,str(problem.Problem_ID))
        if not os.path.exists(problem_dir): # mkdir
            os.mkdir(problem_dir)
        formList = {'Background':form.problemBackground.data,
                    'Description':form.problemDescription.data,
                    'InputFormat':form.problemInput.data,
                    'OutputFormat':form.problemOutput.data,
                    'SampleInput':form.exampleInput.data,
                    'SampleOutput':form.exampleOutput.data}
        for detail in Problem_Details_Class:
            with open(os.path.join(problem_dir,detail) + '.txt','w') as file:
                file.write(formList[detail])
        
        # Add Input and Output data
        inputdatadir = os.path.join(problem_dir,'input')
        if not os.path.exists(inputdatadir): # mkdir
            os.mkdir(inputdatadir)
        for f in request.files.getlist('inputdata'):
            f.save(os.path.join(inputdatadir, f.filename))
        
        outputdatadir = os.path.join(problem_dir,'output')
        if not os.path.exists(outputdatadir): # mkdir
            os.mkdir(outputdatadir)
        for f in request.files.getlist('outputdata'):
            f.save(os.path.join(outputdatadir, f.filename))
        flash('Upload success.')
        return render_template('add_problem.html',form = form)

    return render_template('add_problem.html',form = form)


@app.route('/bbs')
@login_required
def bbs():
    return render_template("index.html",
                           title='论坛')

@app.route('/bbs/post/<post_id>/<replied_id>', methods=['GET', 'POST'])
@login_required
def bbs_post(post_id, replied_id):
    #根据post_id获取相应的post
    post = Post.query.get(int(post_id))
    #不存在的post_id，重定向到全部讨论
    if post == None:
        return redirect(url_for("bbs_posts"))
    edit_delete_form = EditDeleteForm()  #删除、编辑按钮
    comment_form = AddCommentForm()        #添加评论表单
    comment_body = comment_form.comment.data    #读取评论内容
    
    IdentityList = ["管理员", "学生", "教师", "游客"]
    #帖子下的评论（楼层升序）
    comments = Comment.query.filter(Comment.Post_ID == post_id).order_by(Comment.Position.asc())
    #取最后的楼层
    cur_position = 0
    for comment in comments:
        cur_position = comment.Position
    #新发布评论的楼层
    cur_position = cur_position + 1
    
    #当前用户信息（最好整理成单独的函数）
    if current_user.is_authenticated:
        #若用户已登录，使用current_user的身份
        if current_user.Identification == 'Student':
            userIdentity = 1
            user_ID = current_user.Student_ID
            userName = current_user.StuName
        elif current_user.Identification == 'Teacher':
            userIdentity = 2
            user_ID = current_user.Teacher_ID
            userName = current_user.TeaName
        else:
            userIdentity = 0
            user_ID = current_user.Admin_ID
            userName = current_user.AdminName
    else:            
        #测试时允许未登录的游客发帖，使用以下默认身份
        #添加login_required后，游客不可访问此页面
        userIdentity = 3 # 3-Visitor
        user_ID = "000"
        userName = "Visitor"
    
    if(replied_id != '0'):
        replied_comment = Comment.query.get(int(replied_id))
        #不存在的replied_id，重定向到该讨论详情页的评论界面
        if(replied_comment == None):
            return redirect(url_for("bbs_post", post_id = post_id,replied_id = 0))
        message = "@ {pos} 楼：".format(pos = replied_comment.Position)
        #flash(message)
        comment_form.comment.data = message
        replied_position = replied_comment.Position
    else:
        replied_comment = None
        replied_position = 0

    #尚未判断该讨论是否是current_user所发，此时所有人都可以删除讨论
    ### 页面上任一表单的提交都会进入下面的分支，不只是delete_form的提交
    if edit_delete_form.validate_on_submit():
        #！！！待添加：询问是否确认删除！！！
        #！！！在post详情页上方添加评论按钮，跳到页面下方进行评论！！！
        #！！！post详情页的评论旁边添加删除按钮！！！
        #不同表单的submit变量不能重名，否则将无法区分
        #例如：submitDelete和submitComment，若都写成submit，则会只进入第一个分支
        if edit_delete_form.submitDelete.data:
            #删除讨论下的所有评论
            for comment in comments:
                db.session.delete(comment)
                db.session.commit()
            #删除讨论本身
            db.session.delete(post)
            db.session.commit()
            flash("删除成功")
            return redirect(url_for('bbs_mypost'))    
        elif edit_delete_form.submitEdit.data:
            flash("开始编辑")            
            return redirect(url_for('bbs_addpost',saved_post_id = post.Post_ID))
        elif comment_form.submitComment.data:
            #评论失败
            comment_form.comment.data = comment_body
            comment_len = 600
            error_message = "评论失败！"
            comment_error = "内容不得超过600个字符！"
            if len(comment_body) > comment_len:
                error_message = error_message+comment_error
                flash(error_message)
            else:
                #评论成功
                timestamp = datetime.now()
                comment = Comment(Comment_ID = None, Body = comment_body,
                              UserIdentity = userIdentity, User_ID = user_ID,
                              UserName = userName, Timestamp = timestamp,
                              Post_ID = post_id, Topic = post.Topic,
                              Position = cur_position, Replied_ID = replied_id,
                              Replied_Position = replied_position
                              )
                #！！！评论楼层Position尚未设置
                #添加评论
                db.session.add(comment)
                db.session.commit()
                flash("评论成功")
                return redirect(url_for('bbs_post', post_id = post_id, replied_id = 0))
   
    return render_template(
        "post.html",
       title='论坛 - 我的讨论',
       post = post,
       edit_delete_form = edit_delete_form,
       comment_form = comment_form,
       IdentityList = IdentityList,
       comments = comments,
       #当前用户身份
       curUserIdentity = userIdentity,
       curUser_ID = user_ID,
       replied_comment = replied_comment
    )


@app.route('/bbs/mypost/', defaults={'postType' : 'mytopic'})
@app.route('/bbs/mypost/<postType>')
@login_required
def bbs_mypost(postType):
    #设置分页
    page = int(request.args.get('page', 1)) #当前页
    per_page = int(request.args.get('per_page',5))  #每页条数
    #筛选当前用户的讨论记录
    if current_user.is_authenticated:
        #若用户已登录，使用current_user的身份
        if current_user.Identification == 'Student':
            userIdentity = 1
            user_ID = current_user.Student_ID
        elif current_user.Identification == 'Teacher':
            userIdentity = 2
            user_ID = current_user.Teacher_ID
        else:
            userIdentity = 0
            user_ID = current_user.Admin_ID
        #我发起的讨论
        all_posts = Post.query.filter(Post.UserIdentity==userIdentity,Post.User_ID==user_ID).order_by(Post.Timestamp.desc())
        #我参与的讨论
        all_comments = Comment.query.filter(Comment.UserIdentity==userIdentity,Comment.User_ID==user_ID).order_by(Comment.Timestamp.desc())
    else:
        #！！！测试时允许游客查看讨论
        #添加login_required后，游客不可访问此页面
        #全部游客发起的讨论
        all_posts = Post.query.filter(Post.UserIdentity==3).order_by(Post.Timestamp.desc())
        #全部游客参与的讨论
        all_comments = Comment.query.filter(Comment.UserIdentity==3).order_by(Comment.Timestamp.desc())

    #我发起的讨论
    paginate1 = all_posts.paginate(page, per_page, error_out = False)
    posts = paginate1.items
    #我参与的讨论
    paginate2 = all_comments.paginate(page, per_page, error_out = False)
    comments = paginate2.items
  
    return render_template("mypost.html",
                           title='论坛 - 我的讨论',
                           postType = postType,
                           posts = posts,
                           comments = comments,
                           paginate1 = paginate1,
                           paginate2 = paginate2
                           )

@app.route('/bbs/mypost/<postType>/delete/<delete_id>')
@login_required
def bbs_delete(postType, delete_id):
    #删除我发起的讨论
    if postType == 'mytopic':        
        #删除讨论下的所有评论
        comments = Comment.query.filter(Comment.Post_ID == delete_id)
        for comment in comments:
            db.session.delete(comment)
            db.session.commit()
        #删除讨论本身
        post = Post.query.get(delete_id)
        db.session.delete(post)
        db.session.commit()
    #删除我的评论
    else:
        comment = Comment.query.get(delete_id)
        db.session.delete(comment)
        db.session.commit()
   
    #回到我的讨论页面
    flash("删除成功")
    return redirect(url_for('bbs_mypost', postType = postType))

@app.route('/bbs/addpost', defaults={'saved_post_id':None}, methods = ['GET','POST'])    
@app.route('/bbs/addpost/<saved_post_id>', methods = ['GET','POST'])
@login_required
def bbs_addpost(saved_post_id):
    form = AddPostForm()
    #！！！目前form中的course是固定的，应考虑如何使下拉菜单可变
    #！！！编辑草稿时，form中的各项数据的取值要在赋值之前，否则无法取得新数据
    course = form.courseID.data
    topic = form.topic.data
    body = form.post.data
    #编辑之前保存的讨论
    if saved_post_id:
        post_id = saved_post_id
        saved_post = Post.query.get(int(saved_post_id))
        form.courseID.data = saved_post.Course
        form.topic.data = saved_post.Topic
        form.post.data = saved_post.Body
    else:
        post_id = None
        
    if current_user.is_authenticated:
        #若用户已登录，使用current_user的身份
        if current_user.Identification == 'Student':
            userIdentity = 1
            user_ID = current_user.Student_ID
            userName = current_user.StuName
        elif current_user.Identification == 'Teacher':
            userIdentity = 2
            user_ID = current_user.Teacher_ID
            userName = current_user.TeaName
        else:
            userIdentity = 0
            user_ID = current_user.Admin_ID
            userName = current_user.AdminName
    else:            
        #测试阶段允许未登录的游客发帖，使用以下默认身份
        userIdentity = 3 # 3-Visitor
        user_ID = "000"
        userName = "Visitor"
    
    if form.validate_on_submit():
        form.courseID.data = course
        form.topic.data = topic
        form.post.data = body
        #字数超限制
        topic_len = 40
        body_len = 2000
        error_message = '操作失败！'
        topic_error = '主题不得超过40个字符！'
        body_error = '内容不得超过2000个字符！'
        if (topic!=None and len(topic)>topic_len) or (body!=None and len(body)>body_len):
            if topic!=None and len(topic)>topic_len:
                error_message = error_message + topic_error
            if body!=None and len(body)>body_len:
                error_message = error_message + body_error
            flash(error_message)    #错误信息提示
        #发布或保存按钮被单击
        elif form.submit.data or form.save.data:
            if form.submit.data:
                is_posted = True
                flash("发布成功")
            else:
                is_posted = False
                flash("保存成功")
            #编辑的是草稿，则删除草稿
            if saved_post_id:
                db.session.delete(saved_post)
                db.session.commit()
            timestamp = datetime.now()           #获取当前时间
            #Post_ID主键自增
            post = Post(Post_ID = post_id, Course = course, Topic = topic,
                    Body = body, 
                    UserIdentity = userIdentity, User_ID = user_ID,
                    UserName = userName, Timestamp = timestamp,
                    IsPosted = is_posted
                    )
            db.session.add(post)
            #获取自增的Post_ID
            db.session.flush()        
            post_id = post.Post_ID
            db.session.commit()
            #跳转到讨论详情页
            return redirect(url_for('bbs_post', post_id = post_id, replied_id = 0))
    else:
        topic_len = 40
        body_len = 2000
        error_message = '操作失败！'
        topic_error = '主题不得超过40个字符！'
        body_error = '内容不得超过2000个字符！'
        if (topic!=None and len(topic)>topic_len) or (body!=None and len(body)>body_len):
            if topic!=None and len(topic)>topic_len:
                error_message = error_message + topic_error
            if body!=None and len(body)>body_len:
                error_message = error_message + body_error
            flash(error_message)    #错误信息提示

        #end of if submit.data
    # end of if validate_on_submit
    return render_template("addpost.html",
                           title='论坛 - 发起讨论',
                           form = form)

@app.route('/bbs/allposts/', defaults={'postType' : 'all'})
@app.route('/bbs/allposts/<postType>')
@login_required
def bbs_posts(postType):
    page = int(request.args.get('page', 1)) #当前页
    per_page = int(request.args.get('per_page',5))  #每页条数
    #综合讨论区分页
    paginate1 = Post.query.filter(Post.IsPosted == True).order_by(Post.Timestamp.desc()).paginate(page, per_page, error_out = False)
    postsAll = paginate1.items
    #答疑区分页
    paginate2 = Post.query.filter(Post.IsPosted == True, Post.UserIdentity == 2).order_by(Post.Timestamp.desc()).paginate(page, per_page, error_out = False)
    postsQA = paginate2.items
    return render_template("allpost.html",
                           title='论坛 - 查看讨论',
                           postType = postType,
                           postsAll = postsAll,
                           paginate1 = paginate1,
                           postsQA = postsQA,
                           paginate2 = paginate2)

#获取头像
def get_avater(identity, user_id):
    default_avater = "avatar.jpg"
    #管理员
    if(identity == 0):
        avater = Admin.query.get(user_id).Avater
    #学生
    elif(identity == 1):
        avater = Student.query.get(user_id).Avater    
    #教师
    elif(identity == 2):
        avater = Teacher.query.get(user_id).Avater    
    #游客
    else:
        avater = default_avater

    return avater
#将该函数导入html
app.add_template_global(get_avater, "get_avater")

def unique_id(userid, identity):
    if identity == 'Student':
        student = Student.query.filter(Student.Student_ID == userid).first()
        if student:
            return False
        else:
            return True
    else:
        teacher = Teacher.query.filter(Teacher.Teacher_ID == userid).first()
        if teacher:
            return False
        else:
            return True

def has_chinese( string):
    """
    检查整个字符串是否包含中文
    :param string: 需要检查的字符串
    :return: bool
    """
    for ch in string:
        if u'\u4e00' <= ch <= u'\u9fff':
            return True

    return False

def has_space( string):
    """
    检查整个字符串是否包含空格
    :param string: 需要检查的字符串
    :return: bool
    """
    if ' ' in string :
        return True

    return False

@app.route('/register', methods=['GET', 'POST'])
def register():
    # 已登录
    if current_user.is_authenticated:
        return redirect(url_for('index'))

    form = RegisterForm()
    
    if form.validate_on_submit():
        # 数据判断
        flag=False
        if len(form.password2.data)<8 :
            flash("用户密码长度不能小于8")
            flag=True
        
        if len(form.password2.data)>16 :
            flash("用户密码长度不能超过16")
            flag=True
            
        if form.password2.data.isalpha() or form.password2.data.isdigit() :
            flash("用户密码应包含数字与字母")
            flag=True
            
        if has_space(form.userid.data) :
            flash("用户ID不应包含空格")
            flag=True
        if flag == True :
            return render_template("register.html",
                            title='注册',
                            form=form)


            
        if unique_id(form.userid.data, form.identification.data):
            if(form.identification.data == 'Student'):
                if len(form.userid.data)!=7 :
                    flash("学生ID长度应为7")
                    return render_template("register.html",
                            title='注册',
                            form=form)
                user = Student(Phone=form.phone.data,
                               Email=form.email.data,
                               StuName=form.username.data,
                               Student_ID=form.userid.data)
                user.set_password(form.password2.data)
               
            else:
                if len(form.userid.data)!=5 :
                    flash("教师ID长度应为5")
                    return render_template("register.html",
                            title='注册',
                            form=form)
                user = Teacher(Phone=form.phone.data,
                               Email=form.email.data,
                               TeaName=form.username.data,
                               Teacher_ID=form.userid.data)
                user.set_password(form.password2.data)
            
            default_avater='avatar.jpg'
            
            user.Avater=default_avater
            db.session.add(user)
            db.session.commit()
            flash("注册成功!")
            return redirect(url_for('login'))

        else:
            flash("账号已注册!")
    else :
        if form.errors:
            for error in form.errors :
                if error=="userid":
                    error_message = "用户id应为5或7位纯数字"
                    flash(error_message)
            
    return render_template("register.html",
                           title='注册',
                           form=form)


@app.route('/login', methods=['GET', 'POST'])
def login():
    # 已登录
    
    if current_user.is_authenticated:
        return redirect(url_for('index'))

    form = LoginForm()
    if request.method == 'POST' :
        # 根据identification检索数据库
        if form.identification.data == 'Student':
            user = Student.query.filter(
                (Student.Student_ID == form.userid.data)).first()
        elif form.identification.data == 'Teacher':
            user = Teacher.query.filter(
                (Teacher.Teacher_ID == form.userid.data)).first()
        else:
            user = Admin.query.filter(
                (Admin.Admin_ID == form.userid.data)).first()
        if user is None:
            flash("用户不存在")
            return render_template('login.html',
                                   title='登录',
                                   form=form
                                   )
                                   
        flag=user.check_password(form.password.data)
        
        if flag:
            flash("成功登录！")
            login_user(user, form.remember_me.data)     
            return redirect(url_for('index'))
        else:
            error = '用户名或密码输入错误'
            flash(error)
            return render_template('login.html',
                                   title='登录',
                                   form=form
                                   )

    return render_template('login.html',
                           title='登录',
                           form=form
                           )


@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/myinfo/<info_action>', methods=['GET', 'POST'])
@login_required
def myinfo(info_action):
    if info_action == 'editinfo':
        
        form_edit = MyInfoEditForm()
        form_passwordreset = AccountHasPasswordResetForm()

        
        if request.method == "POST":
            # 选择修改密码  
           
            
            if form_passwordreset.validate():

                new_password = form_passwordreset.password2.data
                # 取数据库内账户信息
                if current_user.Identification == 'Student':
                    user = Student.query.filter(
                        Student.Student_ID == current_user.Student_ID).first()
                elif current_user.Identification == 'Teacher':
                    user = Teacher.query.filter(
                        Teacher.Teacher_ID == current_user.Teacher_ID).first()
                else:
                    user = Admin.query.filter(
                        Admin.Admin_ID == current_user.Admin_ID).first()
                # 旧密码输入错误
                flag = user.check_password(form_passwordreset.password_old.data)     
                if not flag:
                    flash("旧密码输入错误")
                    return render_template("myinfo_edit.html",
                                            title='我的信息',
                                            user={
                                                "identification": current_user.get_identification_chinese(),
                                                "id": current_user.get_id(),
                                                "name": current_user.get_name(),
                                                "email": current_user.Email,
                                                "phone": current_user.Phone
                                            },
                                            form_edit=form_edit,
                                            form_passwordreset=form_passwordreset
                                            )
                # 修改密码
                user.set_password(new_password)
                db.session.commit()
                flash("密码已修改")
                # 修改成功返回查看个人信息
                return redirect(url_for('myinfo', info_action='checkdetail'))
            else :
                if form_passwordreset.password_old.data and form_passwordreset.password1.data and form_passwordreset.password2.data :
                    if form_passwordreset.errors:
                        for error in form_passwordreset.errors :
                            print(error)
                            if error=="password1":
                                error_message = "用户密码格式错误"
                                flash(error_message)
                            if error=="password2":
                                error_message = "两次新密码应一样"
                                flash(error_message)
           
            # 选择修改用户名和头像
            if  not form_edit.validate_on_submit():
                
                
                # 新用户名非空则进行修改              
                # 用户未输入用户名，但form内实际存在内容
                new_username=form_edit.username.data
                no_input=r"''"                
                if repr(new_username)!=no_input:
                                       
                    if current_user.Identification == 'Student':
                        user = Student.query.filter(
                            Student.Student_ID == current_user.Student_ID).first()
                        user.StuName=new_username
                        user_identity = 1
                        user_id = current_user.Student_ID
                    elif current_user.Identification == 'Teacher':
                        user = Teacher.query.filter(
                            Teacher.Teacher_ID == current_user.Teacher_ID).first()
                        user.TeaName=new_username
                        user_identity = 2
                        user_id = current_user.Teacher_ID
                    else:
                        user = Admin.query.filter(
                            Admin.Admin_ID == current_user.Admin_ID).first()
                        user.AdminName=new_username
                        user_identity = 0
                        user_id = current_user.Admin
                    
                    #修改post中的username
                    posts = Post.query.filter(Post.UserIdentity == user_identity, Post.User_ID == user_id)
                    for post in posts:
                        post.UserName = new_username
                    #修改comment中的username
                    comments = Comment.query.filter(Comment.UserIdentity == user_identity, Comment.User_ID == user_id)
                    for comment in comments:
                        comment.UserName = new_username
                        
                    db.session.commit()
                    flash("用户名修改成功")
                
                # 新头像非空则进行修改        
                if repr(request.files['profilephoto'].filename)!=no_input :
                    
                    new_profilephoto=request.files['profilephoto']
                    valid_filetype=imghdr.what(new_profilephoto)
                    fname=new_profilephoto.filename                
                    ftype=fname.split('.')[1]     
                    if valid_filetype  :   
                       
                        if current_user.Identification == 'Student':
                            user_ID=current_user.Student_ID
                            user = Student.query.filter(
                                Student.Student_ID == current_user.Student_ID).first()
                        elif current_user.Identification == 'Teacher':
                            user_ID=current_user.TeaName
                            user = Teacher.query.filter(
                                Teacher.Teacher_ID == current_user.Teacher_ID).first()
                        else:
                            user_ID=current_user.AdminName
                            user = Admin.query.filter(
                                Admin.Admin_ID == current_user.Admin_ID).first()
                        size = (128, 128)
                        im=Image.open(new_profilephoto)
                        im.thumbnail(size)
                        #basedir=...\shenjianjiaoxueguanlixitong\app
                        basedir = os.path.dirname(__file__)         
                        filename=user_ID+'.'+ftype
                        upload_path=os.path.join(basedir,'static/user/Avater',filename)
                        #upload_path=os.path.join(basedir)
                        im.save(upload_path)
                        user.Avater=filename
                        db.session.commit()
                        flash("头像修改成功")
                    else :
                        flash("头像修改失败")
                    return redirect(url_for('myinfo', info_action='checkdetail'))

        return render_template("myinfo_edit.html",
                               title='我的信息',
                               user={
                                   "identification": current_user.get_identification_chinese(),
                                   "id": current_user.get_id(),
                                   "name": current_user.get_name(),
                                   "email": current_user.Email,
                                   "phone": current_user.Phone
                                    },
                               form_edit=form_edit,
                               form_passwordreset=form_passwordreset
                               )

    elif info_action == 'checkdetail':
        
        return render_template("myinfo_detail.html",
                               title='我的信息',
                               user={
                                   "identification": current_user.get_identification_chinese(),
                                   "id": current_user.get_id(),
                                   "name": current_user.get_name(),
                                   "email": current_user.Email,
                                   "phone": current_user.Phone
                                    }
                               )


@app.route('/homework/check/<page>')
def homework_check(page):
    if page == 'all':
        return render_template("homework_checklist.html",
                               title='作业 - 收到的作业')
    else:
        return render_template("homework_checkdetail_single.html",
                               title='作业 - 八数码')


@app.route('/account/<action>/<page>', methods=['GET', 'POST'])
def account(action, page):
    form = AccountCheckForm()
    form_password = AccountNoPasswordResetForm()
    
    if action == 'password_reset':
        if page == 'check':
            # 用ID与邮箱来验证
            if form.validate_on_submit():

                # 学生
                user =  Student.query.filter(
                        and_(Student.Student_ID == form.userid.data, Student.Email == form.email.data)).first()
                if user :
                    session['id']=user.Student_ID
                   
                    render_template("account_passwordreset.html",
                                    title='账号 - 身份验证',
                                    current_action='password_reset',
                                    next_page='reset',
                                    form_password = form_password
                                    ) 
                    return redirect(url_for('account',action='password_reset',page='reset'))             
                                 
                # 教师  
                user =  Teacher.query.filter(
                    and_(Teacher.Teacher_ID == form.userid.data, Teacher.Email == form.email.data)).first()
                if user :
                    session['id']=user.Teacher_ID
                    
                    render_template("account_passwordreset.html",
                                title='账号 - 身份验证',
                                current_action='password_reset',
                                next_page='reset',
                                form_password = form_password
                                )
                    return redirect(url_for('account',action='password_reset',page='reset'))
                                        
                else :
                    flash("输入ID与邮箱不匹配")
                    return render_template("account_check.html",
                                title='账号 - 身份验证',
                                current_action='password_reset',
                                next_page='check',
                                form=form)  
            #表单内容错误
            else :
                
                #return redirect(url_for('account',action='password_reset',page='check')) 
                return render_template("account_check.html",
                                        title='账号 - 身份验证',
                                        current_action='password_reset',
                                        next_page='check',
                                        form=form
                                        )
            
        elif page == 'reset':
                        
            ID=session['id']      
            if request.method=='POST':
                if form_password.validate_on_submit() :
                    new_password=form_password.password1.data
                    
                    #学生
                    user =  Student.query.filter(
                            Student.Student_ID == ID).first()
                    if user :
                        
                        user.set_password(new_password)
                        db.session.commit()
                        flash("密码修改成功")
                        return redirect(url_for('login'))

                    #教师
                    user =  Teacher.query.filter(
                        Teacher.Teacher_ID == ID).first()
                    if user :
                        
                        user.set_password(new_password)
                        db.session.commit()
                        flash("密码修改成功")
                        return redirect(url_for('login'))
                                       
                else :
                    
                    return render_template("account_passwordreset.html",
                                    title='账号 - 身份验证',
                                    current_action='password_reset',
                                    next_page='reset',
                                    form_password=form_password
                                    )  
            else:
                return render_template("account_passwordreset.html",
                                    title='账号 - 身份验证',
                                    current_action='password_reset',
                                    next_page='reset',
                                    form_password=form_password
                                    )  


@app.route('/bbs/post/latest')
def postDetail():
    #测试用，获取最新的一条

    post = Post.query.order_by(Post.Timestamp.desc()).first()
    return render_template(
        'post.html',
        post = post
    )

#通知列表
@app.route('/notice/allnotices')
@login_required
def notice_allnotices():
    page = int(request.args.get('page', 1)) #当前页
    per_page = int(request.args.get('per_page', 5))  #每页条数
    paginate2 = Notice.query.order_by(Notice.Timestamp.desc()).paginate(page, per_page, error_out = False)
    notices = paginate2.items
    return render_template("notice_list.html",
                           title='通知 - 查看通知',
                           notices = notices,
                           paginate2 = paginate2)

#通知详情
@app.route('/notice/notice/<notice_id>',methods=['GET', 'POST'])
@login_required
def notice_notice(notice_id):
    #根据notice_id获取相应的notice
    notice = Notice.query.get(int(notice_id))
    #不存在的notice_id，重定向到查看通知页面
    if notice == None:
        return redirect(url_for("notice_allnotices"))
    delete_form = EditDeleteForm()  #删除按钮

    IdentityList = ["管理员", "学生", "教师", "游客"]
    #当前用户信息
    if current_user.is_authenticated:
        #若用户已登录，使用current_user的身份
        if current_user.Identification == 'Student':
            userIdentity = 1
            user_ID = current_user.Student_ID
        elif current_user.Identification == 'Teacher':
            userIdentity = 2
            user_ID = current_user.Teacher_ID
        else:
            userIdentity = 0
            user_ID = current_user.Admin_ID
    else:            
        #测试阶段允许未登录的游客,使用以下默认身份
        userIdentity = 3 # 3-Visitor
        user_ID = "000"

    if delete_form.validate_on_submit():
        db.session.delete(notice)
        db.session.commit()
        flash("删除成功")
        return redirect(url_for('notice_allnotices'))
    return render_template(
        "notice_detail.html",
       title='通知 - 通知详情',
       notice = notice,
       delete_form = delete_form,
       IdentityList = IdentityList,
       #当前用户身份
       curUserIdentity = userIdentity,
       curUser_ID = user_ID,
    )

#发送通知
@app.route('/notice/addnotice', methods = ['GET','POST'])
@login_required
def notice_addnotice():
    form = AddNoticeForm()
    if form.validate_on_submit():
        course = form.courseID.data     #可选课程有限
        topic = form.topic.data
        body = form.post.data
        
        if current_user.is_authenticated:
            #若用户已登录，使用current_user的身份
            if current_user.Identification == 'Student':
                #学生没有权限发通知，直接返回查看通知界面
                return redirect(url_for('notice_allnotices'))
            if current_user.Identification == 'Teacher':
                userIdentity = 2
                user_ID = current_user.Teacher_ID
                userName = current_user.TeaName
            else:
                userIdentity = 0
                user_ID = current_user.Admin_ID
                userName = current_user.AdminName
        else:            
            #游客没有权限发通知，直接返回登录页面
            return redirect(url_for('login'))
        timestamp = datetime.now()           #获取当前时间
        #if form.submit.data:                #发布按钮被单击（此时页面只有一个按钮，无需判断）
        #Notice_ID主键自增
        notice = Notice(Notice_ID = None, Course = course, Topic = topic,
                    Body = body, 
                    UserIdentity = userIdentity, User_ID = user_ID,
                    UserName = userName, Timestamp = timestamp,
                    IsPosted = True)
        db.session.add(notice)
        #获取自增的Notice_ID
        db.session.flush()        
        notice_id = notice.Notice_ID
        db.session.commit()
        #跳转到通知详情页
        return redirect(url_for('notice_notice', notice_id = notice_id))
    else:
        error_message = '发布失败！'
        topic_error = '主题不得超过40个字符！'
        post_error = '内容不得超过600个字符！'
        if form.errors:
            for error in form.errors:
                if error == 'topic':
                    error_message = error_message + topic_error
                elif error == 'post':
                    error_message = error_message + post_error
            flash(error_message)    #错误信息提示

    # end of if    
    return render_template("notice_add.html",
                           title='通知 - 发布通知',
                           form = form)
