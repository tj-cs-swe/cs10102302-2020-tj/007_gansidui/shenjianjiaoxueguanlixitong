from app import db, login
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash

class Student(UserMixin, db.Model):
    __tablename__ = "student"
    Student_ID = db.Column(db.String(7), primary_key=True,unique=True, nullable=False, index = True)
    StuName = db.Column(db.String(16), nullable=False)
    Password = db.Column(db.String(32), nullable=False)
    Email = db.Column(db.String(32))
    Phone = db.Column(db.String(11))
    Identification='Student'
    Avater=db.Column(db.String(256),nullable=True)
    def __repr__(self):
        return '<Student ID:{} Name:{} Password:{} Email:{}>'.format(self.Student_ID, self.StuName, self.Password, self.Email)

    @property
    def is_authenticated(self):
        return True

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.Student_ID)

    def get_name(self):
        return str(self.StuName)

    def get_identification_chinese(self):
        return '学生'

    def set_password(self, password_ori):
        self.Password = generate_password_hash(password_ori, method='pbkdf2:md5')

    def check_password(self, password_check):
        return check_password_hash(self.Password, password_check)

class Teacher(UserMixin, db.Model):
    __tablename__ = "teacher"
    Teacher_ID = db.Column(db.String(7), primary_key=True,unique=True, nullable=False, index = True)
    TeaName = db.Column(db.String(16), nullable=False)
    Password = db.Column(db.String(32), nullable=False)
    Email = db.Column(db.String(32))
    Phone = db.Column(db.String(11))
    Identification='Teacher'
    Avater=db.Column(db.String(256),nullable=True)
    def __repr__(self):
        return '<Teacher ID:{} Name:{} Password:{}>'.format(self.Teacher_ID, self.TeaName, self.Password)
    @property
    def is_authenticated(self):
        return True

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.Teacher_ID)

    def get_name(self):
        return str(self.TeaName)

    def get_identification_chinese(self):
        return '教师'

    def set_password(self, password_ori):
        self.Password = generate_password_hash(password_ori, method='pbkdf2:md5')

    def check_password(self, password_check):
        return check_password_hash(self.Password, password_check)

class Admin(UserMixin, db.Model):
    __tablename__ = "admin"
    Admin_ID = db.Column(db.String(7), primary_key=True,unique=True, nullable=False, index = True)
    AdminName = db.Column(db.String(16), nullable=False)
    Password = db.Column(db.String(32), nullable=False)
    Email = db.Column(db.String(32))
    Phone = db.Column(db.String(11))
    Identification='Admin'
    Avater=db.Column(db.String(256),nullable=True)
    def __repr__(self):
        return '<Admin {} {}>'.format(self.Admin_ID, self.AdminName)

    @property
    def is_authenticated(self):
        return True

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.Admin_ID)

    def get_name(self):
        return str(self.AdminName)

    def get_identification_chinese(self):
        return '管理员'

    def set_password(self, password_ori):
        self.Password = generate_password_hash(password_ori, method='pbkdf2:md5')

    def check_password(self, password_check):
        return check_password_hash(self.Password, password_check)

@login.user_loader
def load_user(id):
    return Student.query.get(int(id))
    
@login.user_loader
def load_Student(id):
    return Student.query.get(int(id))

@login.user_loader
def load_Teacher(id):
    return Teacher.query.get(int(id))

@login.user_loader
def load_Admin(id):
    return Admin.query.get(int(id))


'''
Class Course:
Course_ID      char(8)      unique  not null        primary_key
Course_Name    char(32)     not null
Teacher_ID     char(7)      unique   not null    
'''
class Course(db.Model):
    __tablename__ = "course"
    Course_ID = db.Column(db.String(8), primary_key=True, unique=True, nullable=False, index = True)
    Course_Name = db.Column(db.String(32), nullable=False)
    Teacher_ID = db.Column(db.String(7), db.ForeignKey("teacher.Teacher_ID"))


'''
Class Problem:
Problem_ID            char(5)   not null   unique   auto_increase     primary_key //99999道题不少了吧？
Problem_Name          char(20)    not null
Course_ID             char(8)    not null     foreigner key(Course. Course_ID) 
Teacher_ID            char(7)    not null      foreigner key(Teacher. Teacher_ID) 
'''
class Problem(db.Model):
    __tablename__ = "problem"
    Problem_ID = db.Column(db.Integer, primary_key=True, unique=True, nullable=False, autoincrement = True, index = True)
    Problem_Name = db.Column(db.String(20), nullable=False)
    # Course_ID = db.Column(db.String(8), db.ForeignKey("course.Course_ID"))
    Teacher_ID =  db.Column(db.String(7), db.ForeignKey("teacher.Teacher_ID"))
    Time_Limit =  db.Column(db.Integer, default = 1)        # second
    Memory_Limit = db.Column(db.Integer, default = 128)     # MB


'''
Class Record:
Record_ID（记录id）          char(16)     unique    not null   auto_increase    primary_key
Student_ID（学生ID）         char(7)      not null       foreigner key(Student. Student_ID)
Problem_ID（题目id）         char(5)    not null         foreigner key(Problem. Problem_ID)
Status（WA|AC|....)          char(3)      not null             //'TLE'应该是最长的了吧
Submit_Time                  TIMESTAMP    not null       //此乃时间类型 某教程里‘数据库’一章也有提到
'''
class Record(db.Model):
    __tablename__ = "record"
    Record_ID= db.Column(db.Integer, primary_key=True, unique=True, nullable=False, autoincrement = True, index = True)
    Student_ID = db.Column(db.String(7), db.ForeignKey("student.Student_ID")) 
    # Student = db.relationship('Student', backref='Records', foreign_keys = [Student_ID])

    Problem_ID=  db.Column(db.String(5), db.ForeignKey("problem.Problem_ID"))
    # Problem = db.relationship('Problem', backref='Records', foreign_keys = [Problem_ID])
    
    Status = db.Column(db.String(10), nullable=False)
    Submit_Time = db.Column(db.String(20)) #, default=datetime.utcnow)
    Language = db.Column(db.String(3))
    CodeLength = db.Column(db.Integer)
    Time_Consume = db.Column(db.Float(2))
    Memory_Consume = db.Column(db.Float(2))



class Post(db.Model):
    __tablename__ = "post"
    Post_ID = db.Column(db.Integer, autoincrement = True, primary_key = True, unique = True, nullable = True) #主键自增
    Course = db.Column(db.String(32))
    Topic = db.Column(db.Text)                                    #标题
    Body = db.Column(db.Text)                                     #内容
    UserIdentity = db.Column(db.Integer)                          #发布人身份
    User_ID = db.Column(db.String(7))                             #发布人ID
    UserName = db.Column(db.String(16))                           #发布人昵称
    # Timestamp= db.Column(db.DateTime, default=datetime.utcnow)出错  
    Timestamp = db.Column(db.DateTime)                            #发布时间
    # 发布状态：发布为True，暂存为False
    IsPosted = db.Column(db.Boolean)
    #帖子下的评论
    #SubComments = db.relationship("Comment", backref = "comment_to_post")
    
#end of class Post

class Comment(db.Model):
    __tablename__ = "comment"
    Comment_ID = db.Column(db.Integer, autoincrement = True, primary_key = True, unique = True, nullable = True) #主键自增
    Body = db.Column(db.Text)                                     #内容
    UserIdentity = db.Column(db.Integer)                          #发布人身份
    User_ID = db.Column(db.String(7))                             #发布人ID
    UserName = db.Column(db.String(16))                           #发布人昵称
    Timestamp = db.Column(db.DateTime)                            #发布时间
    #评论所属的帖子
    Post_ID = db.Column(db.Integer)
    Topic = db.Column(db.Text)
    Position = db.Column(db.Integer)        #楼层
    #TopPost = db.relationship("Post", backref = "post_to_comment")
    #若该评论为回复，记录所回复评论的id，否则为0
    Replied_ID = db.Column(db.Integer, index = True)
    Replied_Position = db.Column(db.Integer)

#end of class Comment

class Notice(db.Model):
    __tablename__ = "notice"
    Notice_ID = db.Column(db.Integer, autoincrement = True, primary_key = True, unique = True, nullable = True) #主键自增
    Course = db.Column(db.String(32))
    Topic = db.Column(db.Text)                                    #标题
    Body = db.Column(db.Text)                                     #内容
    UserIdentity = db.Column(db.Integer)                          #发布人身份
    User_ID = db.Column(db.String(7))                             #发布人ID
    UserName = db.Column(db.String(16))                           #发布人昵称
    Timestamp = db.Column(db.DateTime)                            #发布时间
    # 发布状态：发布为True，暂存为False
    IsPosted = db.Column(db.Boolean)

#end of class Notice
