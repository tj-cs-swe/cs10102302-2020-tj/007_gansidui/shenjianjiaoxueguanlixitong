from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
alembic_version = Table('alembic_version', pre_meta,
    Column('version_num', VARCHAR(length=32), primary_key=True, nullable=False),
)

student = Table('student', post_meta,
    Column('Student_ID', String(length=7), primary_key=True, nullable=False),
    Column('StuName', String(length=16), nullable=False),
    Column('Password', String(length=32), nullable=False),
    Column('Email', String(length=32)),
    Column('Phone', String(length=11)),
    Column('Avater', String(length=256)),
)

teacher = Table('teacher', post_meta,
    Column('Teacher_ID', String(length=7), primary_key=True, nullable=False),
    Column('TeaName', String(length=16), nullable=False),
    Column('Password', String(length=32), nullable=False),
    Column('Email', String(length=32)),
    Column('Phone', String(length=11)),
    Column('Avater', String(length=256)),
)

<<<<<<< HEAD
admin = Table('admin', post_meta,
    Column('Admin_ID', String(length=7), primary_key=True, nullable=False),
    Column('AdminName', String(length=16), nullable=False),
=======
teacher = Table('teacher', post_meta,
    Column('Teacher_ID', String(length=7), primary_key=True, nullable=False),
    Column('TeaName', String(length=16), nullable=False),
>>>>>>> 3acab8139c23987d932f59c93beaff06845a722f
    Column('Password', String(length=32), nullable=False),
    Column('Email', String(length=32)),
    Column('Phone', String(length=11)),
    Column('Avater', String(length=256)),
)

<<<<<<< HEAD
=======
comment = Table('comment', post_meta,
    Column('Comment_ID', Integer, primary_key=True),
    Column('Body', Text),
    Column('UserIdentity', Integer),
    Column('User_ID', String(length=7)),
    Column('UserName', String(length=16)),
    Column('Timestamp', DateTime),
    Column('Post_ID', Integer),
    Column('Topic', Text),
    Column('Position', Integer),
    Column('Replied_ID', Integer),
)

>>>>>>> 3acab8139c23987d932f59c93beaff06845a722f

def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
<<<<<<< HEAD
    pre_meta.tables['alembic_version'].drop()
    post_meta.tables['student'].columns['Avater'].create()
    post_meta.tables['teacher'].columns['Avater'].create()
    post_meta.tables['admin'].columns['Avater'].create()
=======
    post_meta.tables['student'].columns['Avater'].create()
    post_meta.tables['admin'].columns['Avater'].create()
    post_meta.tables['teacher'].columns['Avater'].create()
    post_meta.tables['comment'].columns['Position'].create()
>>>>>>> 3acab8139c23987d932f59c93beaff06845a722f


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
<<<<<<< HEAD
    pre_meta.tables['alembic_version'].create()
    post_meta.tables['student'].columns['Avater'].drop()
    post_meta.tables['teacher'].columns['Avater'].drop()
    post_meta.tables['admin'].columns['Avater'].drop()
=======
    post_meta.tables['student'].columns['Avater'].drop()
    post_meta.tables['admin'].columns['Avater'].drop()
    post_meta.tables['teacher'].columns['Avater'].drop()
    post_meta.tables['comment'].columns['Position'].drop()
>>>>>>> 3acab8139c23987d932f59c93beaff06845a722f
